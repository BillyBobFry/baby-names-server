import { MongoClient } from "mongodb";
import dotenv from "dotenv"

dotenv.config({path: '.env.local'})

const {MONGO_PASS, MONGO_USER, MONGO_SERVER, MONGO_DB_NAME} = process.env

const uri = `mongodb+srv://${MONGO_USER}:${MONGO_PASS}@${MONGO_SERVER}/${MONGO_DB_NAME}?retryWrites=true&w=majority`

const client = new MongoClient(uri, {
  native_parser: true,
});

const DB_NAME = MONGO_DB_NAME;
const YEARLY_COLLECTION_NAME = "yearlyname";
const KINDLING_COLLECTION = "kindling";

interface BabyNameQuery {
  name?: string;
  gender?: "boy" | "girl";
  year?: number;
  rank?: number;
  count?: number;
}

export type NameRecord = Pick<Name, "name" | "gender"> & {
  _id: string;
  year: number;
  rank: number;
  count: number;
};

export type KindlingNameRecord = Pick<NameRecord, "name" | "gender">;

export type KindlingRecord = {
  _id?: string;
  kindlingID: string;
  unrated: KindlingNameRecord[];
  rejected: KindlingNameRecord[];
  accepted: KindlingNameRecord[];
};

/**
 * Get the first year in the data.
 * @returns 
 */
export const getMinYear = async (): Promise<number> => {
  const will = await getName("william");
  const willYears = will.map((w) => w.year);
  return Math.min(...willYears);
};

/**
 * Get the most recent year in the data. Assumes that there will always be 
 * at least 3 Williams born each year. If that changes, everything will break.
 * @returns 
 */
export const getMaxYear = async (): Promise<number> => {
  const will = await getName("william");
  const willYears = will.map((w) => w.year);
  return Math.max(...willYears);
};


/**
 * Search for all names matching a search term. Case insensitive.
 * @param searchTerm 
 * @returns 
 */
export const searchForNames = async (searchTerm: string) => {
  const regex = `^${searchTerm.toUpperCase()}`;
  const connection = await client.connect();
  const results = await connection
    .db(DB_NAME)
    .collection(YEARLY_COLLECTION_NAME)
    .aggregate([
      {
        $match: {
          name: {
            $regex: regex,
          },
        },
      },
      {
        $group: {
          _id: {
            name: "$name",
            gender: "$gender",
          },
        },
      },
      {
        $sort: {
          "_id.name": 1,
        },
      },
    ])
    .toArray();

  return await results.map((res: {_id: {name: string, gender: string}}) => res._id);
};

/**
 * Given a name and optional gender, return all records for that name
 * @param name 
 * @param gender 
 * @returns 
 */
export const getName = async (
  name: string,
  gender?: "boy" | "girl"
): Promise<NameRecord[]> => {
  const queryObject: BabyNameQuery = {
    name: name.toUpperCase(),
  };
  if (gender) queryObject.gender = gender;
  const connection = await client.connect();
  const results = await connection
    .db(DB_NAME)
    .collection(YEARLY_COLLECTION_NAME)
    .find(queryObject)
    .toArray();
  return results;
};

export const getKindling = async (
  kindlingID: string,
  gender?: "boy" | "girl"
) => {
  const connection = await client.connect();
  const results = await connection
    .db(DB_NAME)
    .collection(KINDLING_COLLECTION)
    .find({ kindlingID })
    .toArray();

  let kindling = results[0] as KindlingRecord;
  if (gender) {
    kindling = filterGender(kindling, gender);
  }

  return kindling;
};

const filterGender = (
  kindling: KindlingRecord,
  gender: "boy" | "girl"
): KindlingRecord => {
  let { unrated, accepted, rejected } = kindling;

  unrated = unrated.filter((record) => record.gender === gender);
  kindling.unrated = unrated;

  accepted = accepted.filter((record) => record.gender === gender);
  kindling.accepted = accepted;

  rejected = rejected.filter((record) => record.gender === gender);
  kindling.rejected = rejected;

  return kindling;
};

export const getUnratedNames = async (
  kindlingID: string,
  gender?: "boy" | "girl"
): Promise<any> => {
  const connection = await client.connect();
  let kindling = await getKindling(kindlingID);

  if (gender) {
    kindling = filterGender(kindling, gender);
  }

  return kindling.unrated;
};

export const updateNamePopularity = async (years: number, minCount: number) => {
  const connection = await client.connect();
  const maxYear = await getMaxYear();

  /*
   * will's ideal order:
   * for all names that have ever achieved minimumRank,
   * order by their average yearly increase over last 5 years
   */
  connection
    .db(DB_NAME)
    .collection(YEARLY_COLLECTION_NAME)
    .aggregate([
      {
        $match: {
          count: { $gte: minCount },
          year: { $gte: maxYear - years },
        },
      },
      { $sort: { year: 1 } },
      {
        $group: {
          _id: { name: "$name", gender: "$gender" },
          firstCount: { $first: "$count" },
          lastCount: { $last: "$count" },
          averageCount: { $avg: "$count" },
          maxRank: { $max: "$rank" },
        },
      },
      {
        $project: {
          _id: 0,
          name: "$_id.name",
          gender: "$_id.gender",
          averageCount: 1,
          growth: { $divide: ["$lastCount", "$firstCount"] },
        },
      },
      {
        $merge: {
          into: "nameGrowth",
          on: "_id",
          whenMatched: "replace",
          whenNotMatched: "insert",
        },
      },
    ]);
};

export const getNamePopularity = async (
  startYear: number,
  endYear: number,
  minCount: number
) => {
  const connection = await client.connect();
  const maxYear = await getMaxYear();

  /*
   * will's ideal order:
   * for all names that have ever achieved minimumRank,
   * order by their average yearly increase over last 5 years
   */
  const results = connection
    .db(DB_NAME)
    .collection(YEARLY_COLLECTION_NAME)
    .aggregate([
      {
        $match: {
          count: { $gte: minCount },
          year: { $gte: startYear, $lte: endYear },
        },
      },
      { $sort: { year: 1 } },
      {
        $group: {
          _id: { name: "$name", gender: "$gender" },
          firstCount: { $first: "$count" },
          lastCount: { $last: "$count" },
          averageCount: { $avg: "$count" },
          maxRank: { $max: "$rank" },
        },
      },
      {
        $project: {
          _id: 0,
          name: "$_id.name",
          gender: "$_id.gender",
          averageCount: 1,
          growth: { $divide: ["$lastCount", "$firstCount"] },
        },
      },
    ])
    .toArray();

  return results;
};

export const getAllNames = async (minimumRank?: number): Promise<any> => {
  const connection = await client.connect();
  const minYear = await getMinYear();
  const maxYear = await getMaxYear();
  const allYears = [];
  for (let i = minYear; i <= maxYear; i++) {
    allYears.push(i);
  }

  /*
   * will's ideal order:
   * for all names that have ever achieved minimumRank,
   * order by their average yearly increase over last 5 years
   */
  let results = await connection
    .db(DB_NAME)
    .collection(YEARLY_COLLECTION_NAME)
    .aggregate([
      {
        $match: {
          count: { $gte: 50 },
          year: { $gte: maxYear - 5 },
        },
      },
      { $sort: { year: 1 } },
      {
        $group: {
          _id: { name: "$name", gender: "$gender" },
          firstCount: { $first: "$count" },
          lastCount: { $last: "$count" },
          averageCount: { $avg: "$count" },
          maxRank: { $max: "$rank" },
        },
      },
      {
        $project: {
          _id: 1,
          averageCount: 1,
          growth: { $divide: ["$lastCount", "$firstCount"] },
        },
      },
      {
        $project: {
          _id: 1,
          hotness: {
            $multiply: [{ $pow: ["$growth", 0.15] }, { $ln: "$averageCount" }],
          },
        },
      },
      { $sort: { hotness: -1 } },
    ])
    .toArray();
  results = await results.map((record: any) => record._id);
  return results;
};

export const createKindling = async (kindlingID: string): Promise<any> => {
  const allNames = await getAllNames(300);

  const connection = await client.connect();
  const results = await connection
    .db(DB_NAME)
    .collection(KINDLING_COLLECTION)
    .insertOne({
      kindlingID,
      unrated: allNames,
      accepted: [],
      rejected: [],
    });
  return [
    results,
    {
      kindlingID,
      unrated: allNames,
      accepted: [],
      rejected: [],
    },
  ];
};

export const rejectName = async (
  kindlingID: string,
  name: string,
  gender?: "boy" | "girl"
) => {
  const kindling = await getKindling(kindlingID);
  const { unrated, rejected } = kindling;
  const newUnrated = unrated.filter(
    (record: KindlingNameRecord) =>
      record.name !== name || record.gender !== gender
  );

  const toReject = [];
  if (gender) {
    toReject.push({ name, gender });
  } else {
    toReject.push({ name, gender: "boy" });
    toReject.push({ name, gender: "girl" });
  }

  const newRejected = [...rejected, ...toReject];

  const connection = await client.connect();
  const results = await connection
    .db(DB_NAME)
    .collection(KINDLING_COLLECTION)
    .updateOne(
      {
        kindlingID,
      },
      {
        $set: {
          unrated: newUnrated,
          rejected: newRejected,
        },
      }
    );
  return results;
};

export const acceptName = async (
  kindlingID: string,
  name: string,
  gender?: "boy" | "girl"
) => {
  const kindling = await getKindling(kindlingID);
  const { unrated, accepted } = kindling;
  const newUnrated = unrated.filter(
    (record: KindlingNameRecord) =>
      record.name !== name || record.gender !== gender
  );

  const toAccept = gender
    ? [{ name, gender }]
    : [
        { name, gender: "girl" },
        { name, gender: "boy" },
      ];

  const connection = await client.connect();
  const results = await connection
    .db(DB_NAME)
    .collection(KINDLING_COLLECTION)
    .updateOne(
      {
        kindlingID,
      },
      {
        $set: {
          unrated: newUnrated,
          accepted: [...accepted, ...toAccept],
        },
      }
    );
  return results;
};

export const deleteAllAccepted = async (kindlingID: string) => {
  const kindling = await getKindling(kindlingID);
  const { accepted, unrated } = kindling;

  const connection = await client.connect();
  const results = await connection
    .db(DB_NAME)
    .collection(KINDLING_COLLECTION)
    .updateOne(
      {
        kindlingID,
      },
      {
        $set: {
          accepted: [],
          unrated: [...accepted, ...unrated],
        },
      }
    );
  return results;
};

export const deleteAllRejected = async (kindlingID: string) => {
  const kindling = await getKindling(kindlingID);
  const { rejected, unrated } = kindling;

  const connection = await client.connect();
  const results = await connection
    .db(DB_NAME)
    .collection(KINDLING_COLLECTION)
    .updateOne(
      {
        kindlingID,
      },
      {
        $set: {
          accepted: [],
          unrated: [...rejected, ...unrated],
        },
      }
    );
  return results;
};

export const deleteAcceptedName = async (
  kindlingID: string,
  name: string,
  gender?: "boy" | "girl"
) => {
  const kindling = await getKindling(kindlingID);
  const { unrated, accepted } = kindling;
  const newAccepted = accepted.filter(
    (record: KindlingNameRecord) =>
      record.name !== name || record.gender !== gender
  );

  const toUnrate = gender
    ? [{ name, gender }]
    : [
        { name, gender: "girl" },
        { name, gender: "boy" },
      ];

  const connection = await client.connect();
  const results = await connection
    .db(DB_NAME)
    .collection(KINDLING_COLLECTION)
    .updateOne(
      {
        kindlingID,
      },
      {
        $set: {
          accepted: newAccepted,
          unrated: [...unrated, ...toUnrate],
        },
      }
    );
  return results;
};

export const deleteRejectedName = async (
  kindlingID: string,
  name: string,
  gender?: "boy" | "girl"
) => {
  const kindling = await getKindling(kindlingID);
  const { unrated, rejected } = kindling;
  const newRejected = rejected.filter(
    (record: KindlingNameRecord) =>
      record.name !== name || record.gender !== gender
  );

  const toUnrate = gender
    ? [{ name, gender }]
    : [
        { name, gender: "girl" },
        { name, gender: "boy" },
      ];

  const connection = await client.connect();
  const results = await connection
    .db(DB_NAME)
    .collection(KINDLING_COLLECTION)
    .updateOne(
      {
        kindlingID,
      },
      {
        $set: {
          rejected: newRejected,
          unrated: [...unrated, ...toUnrate],
        },
      }
    );
  return results;
};

const NAME_COLLECTION = "names";

export const getNameMeaning = async (name: string, gender: "boy" | "girl") => {
  const connection = await client.connect();
  const results = await connection
    .db(DB_NAME)
    .collection(NAME_COLLECTION)
    .findOne({
      name,
      gender,
    });
  return results;
};

export type Name = {
  name: string;
  gender?: "boy" | "girl";
  meaning?: string;
  origin?: string;
};

export const setNameMeaning = async (
  name: string,
  gender?: "boy" | "girl",
  meaning?: string,
  origin?: string
) => {
  const connection = await client.connect();
  const results = await connection
    .db(DB_NAME)
    .collection(NAME_COLLECTION)
    .updateOne(
      {
        name,
        gender,
      },
      {
        $set: {
          meaning,
          origin,
        },
      },
      { upsert: true }
    );
  return results;
};

export type MatchResult = {
  numberOfParticipants: number;
  matchingNames: Name[];
};

export const getMatches = async (
  kindlingID1: string,
  kindlingID2: string
): Promise<{ numberOfParticipants: number; matchingNames: Name[] } | null> => {
  const connection = await client.connect();
  const results = (await connection
    .db(DB_NAME)
    .collection(KINDLING_COLLECTION)
    .find({
      $or: [{ kindlingID: kindlingID1 }, { kindlingID: kindlingID2 }],
    })
    .toArray()) as KindlingRecord[];

  const numberOfParticipants = results.length;
  if (numberOfParticipants === 1) {
    const { accepted } = results[0];
    return { numberOfParticipants, matchingNames: accepted };
  } else if (numberOfParticipants === 2) {
    const aAccepted = results[0].accepted;
    const bAccepted = results[1].accepted;
    const bKeys = bAccepted.map((record) => `${record.name}${record.gender}`);
    const matchingNames = aAccepted.filter((record) => {
      return bKeys.includes(`${record.name}${record.gender}`);
    });
    return { numberOfParticipants, matchingNames };
  }

  return null;
};
