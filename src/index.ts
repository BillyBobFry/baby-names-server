import express from "express";
import {
  acceptName,
  createKindling,
  deleteAcceptedName,
  deleteAllAccepted,
  deleteRejectedName,
  deleteAllRejected,
  getKindling,
  getMatches,
  getMaxYear,
  getMinYear,
  getName,
  getNameMeaning,
  getNamePopularity,
  getUnratedNames,
  NameRecord,
  searchForNames,
  rejectName,
} from "./dbApi";
import { nanoid } from "nanoid";
import dotenv from "dotenv"

import cors from 'cors'

dotenv.config()

const app = express();
const router = express.Router();

type DataPoint = {
  count: number;
  rank: number;
  year: number;
};

const convertMongoResults = (mongoResults: NameRecord[]): DataPoint[] => {
  return mongoResults.map((nr) => ({
    count: nr.count,
    year: nr.year,
    rank: nr.rank,
  }));
};

app.use(cors())
app.use(router);

router.get("/kindling/id/:id?", (req, res) => {
  let { id } = req.params;
  console.log("fuckingstuff");

  if (!id) {
    id = nanoid(25);
    createKindling(id).then((results) => {
      res.send([id, results]);
    });
  } else {
    getKindling(id)
      .then((data) => {
        res.send({ data });
      })
      .catch((err) => {
        res.send({ isValid: false, err });
      });
  }
});

router.get("/kindling/id/:id/gender/:gender", (req, res) => {
  const { id, gender } = req.params;
  getKindling(id, gender as "boy" | "girl")
    .then((data) => {
      res.send({ data });
    })
    .catch((err) => {
      res.send({ isValid: false, err });
    });
});

router.post("/kindling/id/:id", (req, res) => {
  const { id, name } = req.params;
  createKindling(id).then((results) => {
    res.status(200).send();
  });
});

router.put("/kindling/id/:id/reject/:reject/:gender?", (req, res) => {
  const { id, reject, gender } = req.params;
  rejectName(id, reject, gender as "boy" | "girl" | undefined)
    .then((response) => res.send(200))
    .catch((err) => res.status(500).send(err));
});

router.delete("/kindling/id/:id/reject", (req, res) => {
  const { id } = req.params;
  deleteAllRejected(id)
    .then(() => res.send(200))
    .catch((err) => res.status(500).send(err));
});

router.delete("/kindling/id/:id/reject/:reject/:gender?", (req, res) => {
  const { id, reject, gender } = req.params;
  deleteRejectedName(id, reject, gender as "boy" | "girl" | undefined)
    .then(() => res.send(200))
    .catch((err) => res.status(500).send(err));
});

router.put("/kindling/id/:id/accept/:accept/:gender?", (req, res) => {
  const { id, accept, gender } = req.params;
  acceptName(id, accept, gender as "boy" | "girl" | undefined)
    .then(() => res.send(200))
    .catch((err) => res.status(500).send(err));
});

router.delete("/kindling/id/:id/accept", (req, res) => {
  const { id } = req.params;
  deleteAllAccepted(id)
    .then(() => res.send(200))
    .catch((err) => res.status(500).send(err));
});

router.delete("/kindling/id/:id/accept/:accept/:gender?", (req, res) => {
  const { id, accept, gender } = req.params;
  deleteAcceptedName(id, accept, gender as "boy" | "girl" | undefined)
    .then(() => res.send(200))
    .catch((err) => res.status(500).send(err));
});

router.get("/kindling/id/:id/unrated/gender/:gender", (req, res) => {
  const { id, gender } = req.params;
  getUnratedNames(id, gender as "boy" | "girl")
    .then((results) => res.send(results))
    .catch((err) => res.status(500).send(err));
});
router.get("/kindling/id/:id/unrated", (req, res) => {
  const { id, name } = req.params;
  getUnratedNames(id)
    .then((results) => res.send(results))
    .catch((err) => res.status(500).send(err));
});

router.get("/kindling/id/:id1/matches/:id2", async (req, res) => {
  const { id1, id2 } = req.params;
  const { limit } = req.query;
  const results = await getMatches(id1, id2);
  console.log(results.matchingNames);
  const namesWithData = results.matchingNames.map(async (name) => {
    const meaningData = await getNameMeaning(name.name, name.gender);
    return { ...name, ...meaningData };
  });

  const returnData = await Promise.all(namesWithData);
  res.send({
    numberOfParticipants: results.numberOfParticipants,
    nameData: returnData,
  });
});

router.get("/births/:name/gender/:gender", (req, res) => {
  const { name, gender } = req.params;
  getName(name, gender as "boy" | "girl").then((results) =>
    res.send(convertMongoResults(results))
  );
});

router.get("/births/:name", (req, res) => {
  const { name } = req.params;
  getName(name).then((results) => res.send(results));
});

router.get("/names/popularity", (req, res) => {
  const { startYear, endYear, minCount } = req.query;
  getNamePopularity(Number(startYear), Number(endYear), Number(minCount)).then(
    (results) => {
      res.send(results);
    }
  );
});

router.get("/names/:search", (req, res) => {
  const { search } = req.params;
  searchForNames(search).then((results) => res.send(results));
});

router.get("/names/:name/gender/:gender/meaning", (req, res) => {
  const { name, gender } = req.params;
  getNameMeaning(name, gender as "boy" | "girl").then((results) =>
    res.send(results || {})
  );
});

router.get("/min-year", async (req, res) => {
  const minYear = await getMinYear();
  console.log(minYear);
  res.send({ minYear });
});
router.get("/max-year", async (req, res) => {
  const maxYear = await getMaxYear();
  console.log(maxYear);
  res.send({ maxYear });
});
router.get("/minmax-years", async (req, res) => {
  const minmaxYears = [getMinYear(), getMaxYear()];
  const [minYear, maxYear] = await Promise.all(minmaxYears);
  res.send({ minYear, maxYear });
});

router.get("/*", (req, res) => {
  res.send(`404 mate sorry, can't find the url ${req.url}`);
});

app.get("/*", (req, res) => {
  res
    .status(404)
    .send(
      `404, didn't even hit the router mate. Can't find the url ${req.url}.`
    );
});

app.listen(process.env.PORT, ()=>{console.log(`listening on ${process.env.PORT}`)});
