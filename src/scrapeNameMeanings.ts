import { JSDOM } from "jsdom";
import axios from "axios";
import { getAllNames, setNameMeaning } from "./dbApi";

const getName = async () => {
  const allNames = (await getAllNames(500)) as any[];

  const scrapeName = async (nameObject: { name: string; gender: string }) => {
    console.log(`scraping ${nameObject.name}`);
    const response = await axios.get(
      `https://www.babynames.com/name/${nameObject.name.toLowerCase()}`
    );
    const dom = await new JSDOM(response.data);

    const meaningElements = dom.window.document.querySelectorAll(
      ".name-meaning"
    );

    const meanings: { meaning: string | null; origin: string | null } = {
      meaning: null,
      origin: null,
    };

    meaningElements.forEach((m: any) => {
      const split = m.textContent.split(": ");
      const key = split[0].toLowerCase() as "meaning" | "origin";
      meanings[key] = split[1];
    });

    setNameMeaning(
      nameObject.name,
      nameObject.gender as "boy" | "girl",
      meanings.meaning,
      meanings.origin
    );
  };

  allNames.forEach((name: any) => {
    const randomTimeout = Math.random() * 5000000;
    setTimeout(() => {
      scrapeName(name);
    }, randomTimeout);
  });
};
getName();
